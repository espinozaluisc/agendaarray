package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {
    private TableLayout tblLista;
    private ArrayList<Contacto> contactos;
    private ArrayList<Contacto> filther;
    private Button btnNuevo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        tblLista=(TableLayout)findViewById(R.id.tblLista);

        Bundle bundleObject = getIntent().getExtras();
        contactos=(ArrayList<Contacto>)bundleObject.getSerializable("contactos");
        filther= contactos;
        Button btnNuevo=(Button)findViewById(R.id.btnNuevo);
        cargarContactos();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        btnNuevo.setOnClickListener(btnNuevo());

    }
    private View.OnClickListener btnNuevo()
    {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendData();
            }
        };
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            //si el usuario presiona el boton back regresara la lista
            //pero no editara
            sendData();
        }
        return super.onKeyDown(keyCode, event);
    }


    public void sendData()
    {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(getString(R.string.listContactos) ,this.filther);
        bundle.putInt(getString(R.string.action), 0);
        intent.putExtras(bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
        Log.e(this.getClass().getName(), "back button pressed");
    }

    private void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for (int x=0;x<filther.size(); x++){
            if(filther.get(x).getNombre().contains(s))
                list.add(filther.get(x));
        }
        contactos = list;
        tblLista.removeAllViews();
        cargarContactos();
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
            buscar(s);
            return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    public void cargarContactos(){
        for(int x=0;x<contactos.size();x++){
            Contacto c = new Contacto(contactos.get(x));
            TableRow nRow=new TableRow(ListActivity.this);
            TextView nText = new TextView(ListActivity.this);
            nText.setText(c.getNombre());

            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nText.setTextColor((c.isFavorito())? Color.BLUE: Color.BLACK);
            nRow.addView(nText);

            Button nButton = new Button(ListActivity.this);
            nButton.setText(R.string.accver);
            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT,6);
            nButton.setTextColor(Color.BLACK);

            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent();
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto", (Contacto) v.getTag(R.string.contacto_g));
                    bundle.putSerializable(getString(R.string.listContactos) , filther);
                    bundle.putInt(getString(R.string.action), 1);
                    i.putExtras(bundle);
                    setResult(RESULT_OK, i);
                    finish();

                }
            });
            nButton.setTag(R.string.contacto_g,c);
            nButton.setTag(R.string.contacto_g_index,x);
            nRow.addView(nButton);
            Button btnElimiar = new Button(getApplicationContext());
            btnElimiar.setText(R.string.eliminar);
            btnElimiar.setTextColor(Color.BLACK);
            btnElimiar.setTextSize(TypedValue.COMPLEX_UNIT_PT, 6);
            btnElimiar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    long id = Long.parseLong(String.valueOf(v.getTag(R.string.index)));
                    elimianarContacto(id);
                }
            });
            btnElimiar.setTag(R.string.index, c.getID());
            nRow.addView(btnElimiar);

            tblLista.addView(nRow);

        }
    }
    private void elimianarContacto(long id)
    {
        for(int x = 0; x <filther.size(); x++)
        {
            if(filther.get(x).getID() == id)
            {
                filther.remove(x);
                break;
            }
        }
        contactos = filther;
        tblLista.removeAllViews();
        cargarContactos();

    }

}
