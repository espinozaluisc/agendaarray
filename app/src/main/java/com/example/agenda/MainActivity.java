package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
    private EditText edtNombre;
    private EditText edtTelefono;
    private EditText edtTelefono2;
    private EditText edtDireccion;
    private EditText edtNotas;
    private CheckBox cbxFavorito;
    private Contacto saveContact;
    Button btnGuardar;
    Button btnLimpiar;
    Button btnListar;
    private int savedIndex;
    private Button btnCerrar;
    int index = 1;

    private boolean isEdit = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtNombre = (EditText) findViewById(R.id.txtNombre);
        edtTelefono = (EditText) findViewById(R.id.txtTel1);
        edtTelefono2 = (EditText) findViewById(R.id.txtTel2);
        edtDireccion = (EditText) findViewById(R.id.txtDomicilio);
        edtNotas = (EditText) findViewById(R.id.txtNotas);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtDireccion.getText().toString().matches("") || edtNombre.getText().toString().matches("")
                        || edtTelefono.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, R.string.msgerror, Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();

                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDomicilio(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNombre.getText().toString());
                    nContacto.setFavorito(cbxFavorito.isChecked());
                    if (!isEdit)
                    {
                        nContacto.setID(index);
                        contactos.add(nContacto);
                        index++;
                    } else {
                        nContacto.setID(saveContact.getID());
                        editContact(nContacto);
                        isEdit = false;
                        saveContact = null;
                    }
                    limpiar();
                    Toast.makeText(getApplicationContext(), R.string.mensaje,
                            Toast.LENGTH_SHORT).show();


                }
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    private void editContact(Contacto contacto) {
        for (int x = 0; x < this.contactos.size(); x++) {
            if (this.contactos.get(x).getID() == contacto.getID()) {
                this.contactos.set(x, contacto);
            }
        }
    }

    public void limpiar() {
        saveContact = null;
        edtNotas.setText("");
        edtDireccion.setText("");
        edtTelefono2.setText("");
        edtTelefono.setText("");
        edtNombre.setText("");
        cbxFavorito.setChecked(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent != null) {
            Bundle oBundle = intent.getExtras();
            int typeAction = oBundle.getInt(getString(R.string.action));
            this.contactos.clear();
            this.contactos.addAll((ArrayList<Contacto>)
                    oBundle.getSerializable(getString(R.string.listContactos)));
            if (typeAction == 1) {


                saveContact = (Contacto) oBundle.getSerializable("contacto");
                savedIndex = oBundle.getInt("index");
                edtNombre.setText(saveContact.getNombre());
                edtTelefono.setText(saveContact.getTelefono1());
                edtTelefono2.setText(saveContact.getTelefono2());
                edtDireccion.setText(saveContact.getDomicilio());
                edtNotas.setText(saveContact.getNotas());
                cbxFavorito.setChecked(saveContact.isFavorito());
                isEdit = true;
            } else {
                limpiar();
            }
        }
    }
}